// const webpack = require("webpack");
const path = require("path");
const fs = require("fs");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const InjectPlugin = require('webpack-inject-plugin').default;

const isObject = (item) => {
	return (item && typeof item === 'object' && !Array.isArray(item));
};

const merge = (target, ...sources) => {
	if (!sources.length) return target;
	const source = sources.shift();

	if (isObject(target) && isObject(source)) {
		for (const key in source) {
			if (isObject(source[key])) {
				if (!target[key]) Object.assign(target, { [key]: {} });
				merge(target[key], source[key]);
			} else if (Array.isArray(source[key])) {
				target[key] = [...target[key], ...source[key]]
			} else {
				Object.assign(target, { [key]: source[key] });
			}
		}
	}

	return merge(target, ...sources);
}

const generateBuildTemplate = (resolveAppPath) => {
	const package = require(resolveAppPath("package.json"));

	const revision = (() => {
		try {
			const head = fs.readFileSync(resolveAppPath(".git/HEAD")).toString().trim();
			const revision = head.includes(":")
			? fs
				.readFileSync(resolveAppPath(`.git/${head.substring(5)}`))
				.toString()
				.trim()
			: head;
			return revision;
		} catch (e) {
			return null;
		}
	})();

	const fonts = (() => {
		try {
			const fonts = {
				directory: resolveAppPath("src/assets/fonts"),
				buildDirectory: "fonts",
			};
			fonts.styles = fs.readdirSync(fonts.directory)
			.reduce(
				(fullStyles, filename) => (
					fullStyles +
					`@font-face { 
						font-family: "${path.parse(filename).name}"; 
						src: url("${fonts.buildDirectory}/${filename}"); 
						font-weight: ${filename.includes("Bold") ? "bold" : "normal"};
						font-style: ${filename.includes("Italic") ? "italic" : "normal"};
					}\n\n`
				), ""
			);
			return fonts;
		} catch (e) {
			// console.log(e);
			return null;
		}
	})();

	const copyPattern = [
		(fonts && { from: fonts.directory, to: fonts.buildDirectory }),
	].filter(Boolean);

	return {
	  entry: resolveAppPath("src/index.js"),
	  output: {
		path: resolveAppPath("build"),
		filename: "bundle.js",
	  },
	  resolve: {
		modules: [resolveAppPath("src"), resolveAppPath("node_modules")],
		fallback: {
			"@8ocorg/communicator": require.resolve("@8ocorg/communicator"),
			"@8ocorg/screenshot": require.resolve("@8ocorg/screenshot"),
		},
	  },
	  module: {
		rules: [
		  {
			test: /\.(js|jsx)$/,
			loader: "babel-loader",
			exclude: /node_modules/,
			options: {
			  presets: [require.resolve("@babel/preset-react")],
			},
		  },
		],
	  },
	  devServer: {
		compress: true,
		hot: true,
		host: "localhost",
		historyApiFallback: true,
		port: 3000,
	  },
	  plugins: [
		new HtmlWebpackPlugin({
		  templateContent: () =>
			`<!DOCTYPE html>
	
			<!-- ${package.codeComment} -->
			
			<html>
			  <head>
				<meta charset='UTF-8' name='viewport' content='width=device-width, initial-scale=1'>
				<title>8o-COLLECTIVE</title>
				<base href='/'>
	
				<style>
				body { background-color: black; }
	
				${fonts?.styles || ""}
				</style>
			  </head>
			  <body>
				<div id='root'></div>
			  </body>
			</html>
			`,
		  filename: "index.html",
		  minify: {
			collapseWhitespace: false,
			removeComments: false,
		  },
		}),
		new InjectPlugin(() => `
			window.host="${process.env.HOST}";
			window.domain="${process.env.DOMAIN ?? process.env.HOST}";
			window.revision="${revision}";

			require("@8ocorg/communicator").enableCommunication();

			${!package.special && `require("@8ocorg/screenshot").enableScreenshot();`}
		`),
		...(copyPattern.length ? [new CopyPlugin({ patterns: copyPattern })] : []),
	  ],
	};
}

module.exports = (resolveAppPath, buildConfig) => merge(generateBuildTemplate(resolveAppPath), buildConfig);